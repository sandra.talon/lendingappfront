import { Book } from './book.model';

export const BOOKS: Book[] = [
  { id: 11, title: 'Dr Nice', author: 2, series: 4 },
  { id: 12, title: 'Narco', author: 2, series: 4 },
  { id: 13, title: 'Bombasto', author: 2, series: 4 },
  { id: 14, title: 'Celeritas', author: 2, series: 4 },
  { id: 15, title: 'Magneta', author: 2, series: 4 },
  { id: 16, title: 'RubberMan', author: 2, series: 4 },
  { id: 17, title: 'Dynama', author: 2, series: 4 },
  { id: 18, title: 'Dr IQ', author: 2, series: 4 },
  { id: 19, title: 'Magma', author: 2, series: 4 },
  { id: 20, title: 'Tornado', author: 2, series: 4 }
];
