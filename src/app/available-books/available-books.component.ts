import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';


import { BookService } from './book.service';
import { Book } from '../model/book.model'

@Component({
  selector: 'app-available-books',
  templateUrl: './available-books.component.html',
  styleUrls: ['./available-books.component.css']
})
export class AvailableBooksComponent implements OnInit {

  books: Book[];

  constructor(private location: Location, private bookService: BookService) { }

  ngOnInit() {
    this.getBooks();
  }

  getBooks(): void {
    this.bookService.getBooks().subscribe(books => this.books = books);
  }

  add(title: string, author: number, series: number): void {
    title = title.trim();
    if (!title || !author) { return; }
    this.bookService.addBook({ title, author, series } as Book)
      .subscribe(book => {
        this.books.push(book);
      });
  }

  goBack(): void {
    this.location.back();
  }

  delete(book: Book): void {
    this.books = this.books.filter(h => h !== book);
    this.bookService.deleteBook(book).subscribe();
  }

}
