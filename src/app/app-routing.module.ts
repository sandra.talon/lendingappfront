import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AvailableBooksComponent } from './available-books/available-books.component';
import { DashboardComponent } from './dashboard/dashboard.component'
import { BookDetailsComponent } from './book-details/book-details.component';

const routes: Routes = [
  { path: 'available-books', component: AvailableBooksComponent },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'details/:id', component: BookDetailsComponent },
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
