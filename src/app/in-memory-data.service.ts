import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Book } from './model/book.model';

@Injectable({
  providedIn: 'root',
})
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    const books = [
  { id: 11, title: 'Dr Nice', author: 2, series: 4 },
  { id: 12, title: 'Narco', author: 2, series: 4 },
  { id: 13, title: 'Bombasto', author: 2, series: 4 },
  { id: 14, title: 'Celeritas', author: 2, series: 4 },
  { id: 15, title: 'Magneta', author: 2, series: 4 },
  { id: 16, title: 'RubberMan', author: 2, series: 4 },
  { id: 17, title: 'Dynama', author: 2, series: 4 },
  { id: 18, title: 'Dr IQ', author: 2, series: 4 },
  { id: 19, title: 'Magma', author: 2, series: 4 },
  { id: 20, title: 'Tornado', author: 2, series: 4 }
    ];
    return {books};
  }

  // Overrides the genId method to ensure that a book always has an id.
  // If the books array is empty,
  // the method below returns the initial number (11).
  // if the books array is not empty, the method below returns the highest
  // book id + 1.
  genId(books: Book[]): number {
    return books.length > 0 ? Math.max(...books.map(book => book.id)) + 1 : 11;
  }
}
